//
//Author: Saqib Khalil Yawar
//EMAIL: syawar@syawar.com
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.

selectedId = -1

//This function handles the update of the badge text and notifications
function show(prevVal) {
	//get result for the rest api query
    var response = ajaxGet();
    var content = '';
	//check if jQuery activated
    if (jQuery) {
        content = "Alarm(s):" + response;
    } else {
        content = "JQuery not loaded";
    }
    
    //do the notificaitons
    doNotifications(prevVal,response,content);
	
	//update badge text number in tool bar
    chrome.browserAction.setBadgeText({
        "text": response,
        tabId: selectedId
    });
    return response;
}

//notification generator  and creation
function doNotifications(prevVal,response,content){
    //if notification activated and increase in alarms, send notifications
    if ((prevVal != response) && JSON.parse(localStorage.isNotifActivated)) {
        if (isNaN(prevVal)) {
            prevVal = 0;
        }
        debugLogger("generating notification...should show...");
        debugLogger("the notification are set to "+localStorage.isNotifActivated);
        var time = /(..)(:..)/.exec(new Date()); // The prettyprinted time.
        var hour = time[1] % 12 || 12; // The prettyprinted hour.
        var period = time[1] < 12 ? 'a.m.' : 'p.m.'; // The period of the day.
        var notification = window.webkitNotifications.createNotification(
            'img/48.png', // The image.
            hour + time[2] + ' ' + period, // The title.
            content + " | Previous Alarm(s):" + prevVal // The body.
        );

        //the url to navigate to
        var newURL = getConnector() + '://' + localStorage.opennmsip + '/opennms/login.jsp';

        //url navigation
        notification.addEventListener('click', function() {
            notification.cancel();
            window.open(newURL);
        });

        notification.show();
        //time to cancel the notification after 2 sec
        setTimeout(function(){
            notification.cancel();
        },5000);
    } else {
        debugLogger("the notification are set to "+localStorage.isNotifActivated);
    }
}

//formulate database query
function databaseQuery(){
     var theQuery="";
    debugLogger("The custom query activated="+localStorage.customquery);
    //check if there is a restquery
    if(!JSON.parse(localStorage.customquery)){
        debugLogger("default query"+localStorage.restquery);
        theQuery="alarms?limit=0&severity="+localStorage.eventType;
    }
    else {
        var encodedQuery=encodeURI(localStorage.restquery.trim());
        debugLogger("The custom query"+localStorage.restquery);
        theQuery=encodedQuery;
    }
    debugLogger("The query executed="+theQuery);
	return theQuery;


}
//logging in console for debugging purposes
function debugLogger(info){
      if (JSON.parse(localStorage.debugMode)){
          console.log(info);
      }
}

//ajax response of the rest api query
function ajaxGet() {
    var theUrl = getConnector() + "://" + localStorage.username + ":" + localStorage.password + "@" + localStorage.opennmsip + "/opennms/rest/";
    theUrl=theUrl+databaseQuery();
    debugLogger("the url used for ajax get="+theUrl);
    try{
        var response = $.ajax({
            type: "GET",
            url: theUrl,
            async: false
        }).responseText;
        xmlDoc = $.parseXML( response );
        $xml = $( xmlDoc );
        $title = $xml.find( localStorage.parentnode );
        var result = $title.attr(localStorage.countername);
        debugLogger("The result from rest query="+result+" for the counter="+localStorage.countername+" on the xml node="+localStorage.parentnode);

        //setting may cause return to be undefined
        if (result == undefined){
            debugLogger("The value returned was undefined");
            alert("Undefined Returned: check the query or the other settings (parent node most commonly)!");
        }
        return result;
        }
    catch(e){
        debugLogger("Could not get the results because:\n1)ssl issue or\n2)configuration issue");
        //alert("Could not get the results because:\n1)ssl issue or\n2)configuration issue");
        debugLogger(e);
        //if settings done but error persists...probably misconfiguration or ssl issue
        if(e.code == "101" & localStorage.username != '' & localStorage.password != '' & localStorage.opennmsip != '' ){
            debugLogger( "caught code 101..ssl network error on cross origin request...check ssl!" );
            alert("NETWORK_ERROR::CHECK OPTIONS AND SSL CERT!!!");
        }
        else{
            if(!JSON.parse(localStorage.isOptionLoaded)){
                //on config issue load the options page
                loadOptions();
                localStorage.isOptionLoaded = true;
            }
            else{
                debugLogger("Options Loaded...Not the exception..printing:"+ e.toString());
            }

        }

        return undefined;
    }

}

//load the options page
function loadOptions(){
    var optionsUrl = chrome.extension.getURL('form/form.html');

    chrome.tabs.query({url: optionsUrl}, function(tabs) {
        if (tabs.length) {
            chrome.tabs.update(tabs[0].id, {active: true});
        } else {
            chrome.tabs.create({url: optionsUrl});
        }
    });
}

//initialization of the used variables
if (!localStorage.isInitialized) {
    localStorage.debugMode = false;//debug mode default off
    localStorage.isInitialized = true; // The option initialization.
    localStorage.linktype = true; //http or https
    localStorage.isNotifActivated = false; // The notifcations activation.
    localStorage.frequency= 1; // The display frequency, in minutes.
    localStorage.eventType="MAJOR";//eventType initialization
    localStorage.customquery = false; //customquery
    localStorage.username = '';//the default username
    localStorage.password = ''; // default password
    localStorage.opennmsip= ''; // the default opennms ip
    localStorage.restquery="alarms?limit=0&severity="+localStorage.eventType;//default rest query
    localStorage.parentnode="alarms";//default parent node
    localStorage.countername="totalCount";//the counter to display
    localStorage.isOptionLoaded = false;

}

//the base url to be used for the query
var prevVal = show(prevVal);



var interval = 0; // The display interval, in minutes.
setInterval(function () {
    interval++;

    if (
        localStorage.frequency <= interval) {
        prevVal = show(prevVal);
        interval = 0;
    }
}, 60000);

function getConnector(){
       var connector = 'https';
        if (!JSON.parse(localStorage.linktype)){
            connector = 'http';
        }
    return connector;
}
//action event listener to implement the "got" configured OpenNMS on icon click in toolbar 
chrome.browserAction.onClicked.addListener(function (activeTab) {
    var newURL = getConnector() + '://' + localStorage.opennmsip + '/opennms/login.jsp';
    chrome.tabs.create({
        url: newURL
    });
});

