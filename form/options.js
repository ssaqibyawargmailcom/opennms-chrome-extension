//
//Author: Saqib Khalil Yawar
//EMAIL: syawar@syawar.com
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
/*
 Grays out or [whatever the opposite of graying out is called] the option
 field.
 */
function ghost(isDeactivated) {
    options.style.color = isDeactivated ? 'graytext' : 'black';
    // The label color.
    options.restquery.disabled(isDeactivated); // The control manipulability.
}


//Handles page load
window.addEventListener('load', function() {
    // Initialize the option controls.

    options.debugMode.checked = JSON.parse(localStorage.debugMode); //debug mode for logging

    options.isNotifActivated.checked = JSON.parse(localStorage.isNotifActivated);//notifications

    options.frequency.value = localStorage.frequency;

    // The display frequency, in minutes.
    options.username.value = localStorage.username; //username

    options.password.value = localStorage.password; //password

    options.linktype.checked = JSON.parse(localStorage.linktype); //link type

    options.opennmsip.value = localStorage.opennmsip; //onms ip

    options.restquery.value = localStorage.restquery; //requestquery

    options.eventType.value = localStorage.eventType; //rmajor/minor

    options.customquery.checked = JSON.parse(localStorage.customquery); //custom query

    options.parentnode.value = localStorage.parentnode; //the xml file parent node

    options.countername.value = localStorage.countername; //the counter to display



    // Set the display
    saveOptions();

});

//handles changes to options
function saveOptions(){
// Set the display

    options.isNotifActivated.onchange = function() {//notification activation
        localStorage.isNotifActivated = options.isNotifActivated.checked;
    };
    options.frequency.onchange = function() {//frequency of monitoring and/or notification
        localStorage.frequency = options.frequency.value;
    };

    options.username.onchange=function(){//username
        localStorage.username = options.username.value;
    };

    options.password.onchange=function(){//password
        localStorage.password = options.password.value;
    };

    options.linktype.onchange=function(){//type of link
        localStorage.linktype = options.linktype.checked;
    };

    options.opennmsip.onchange=function(){//ip of the machine
        localStorage.opennmsip = options.opennmsip.value;
    };

    options.restquery.onchange=function(){//rest query to be run
        localStorage.restquery = options.restquery.value;

    };

    options.eventType.onchange=function(){//event to monitor
        localStorage.eventType = options.eventType.value;
    };

    options.customquery.onchange=function(){//custom query
        localStorage.customquery = options.customquery.checked;

    };

    options.debugMode.onchange=function(){//debug mode
        localStorage.debugMode = options.debugMode.checked;

    };

    options.parentnode.onchange=function(){//parent node
        localStorage.parentnode = options.parentnode.value;

    };

    options.countername.onchange=function(){//counter name
        localStorage.countername = options.countername.value;

    };


}


document.querySelector('#save').addEventListener('click', function(){
    console.log("Save button clicked");
    saveOptions();
    console.log("trying reloading");
    chrome.runtime.reload();
});
