==========================
 OpenNMS Chrome Extension
==========================

We develop that extension because we intensively use OpenNMS to monitor our infrastructure.
Plus, we wanted to be proactive in the monitoring of our servers, so we place in the right
corner of our browser a little reminder of what is going on.

This extension displays the number of broken or down servers on the infrastructure.

To use at your own risk.
==========================

We develop it, we are using it, but we deliver this extension to you without any garanty
that it will work for you.

THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
PARTICULAR PURPOSE.

Important Note
==============

Always clone master if you wish to have the best extension at that time. Since the other branches are
mostly for development purposes, they are merged once they are complete.

Knowledge Base
==============

The wiki for this extension is at https://bitbucket.org/ssaqibyawargmailcom/opennms-chrome-extension/

What is it?
============

This extension allows the user to set popup notifications for a particular count (e.g alarms) to be seen
when google chrome is active. The extension relies on the OpenNMS rest interface, therefore suffers the
same limitation of information that the interface has. The details can be found on http://www.opennms.org/wiki/ReST .
The settings allow a user to specify a query that can return a custom count instead of a predefined one.

Requirements:
============

The extension requires a valid ssl certificate to be present within the opennms keytool, or chrome loaded in
insecure mode, to be able to function properly. If you get an "SSL Error" when you first navigate to your OpenNMS,
after loading chrome, the extension will not be able to report anything until you click "Proceed Anyways", to create
a security exception in chrome, which normally lasts only until Chrome is closed, and the whole thing has to be done
again on reload.
Therefore, it is recommended that your OpenNMS has an accepted SSL certificate.

Default Behavior:
================

By default, the extension get the count of all alarms with the severity set to MAJOR and the custom query
option set to "No" and the pop-up notification option set to "Yes". If you wish to add a custom query, you
will first need to enable the custom query option, which in-turn will ignore the other query setting, except
the custom query, xml node name and counter name.

The parent node name is basically the node name in the xml result of the rest query and the counter name is the
counter that you want to be notified about. Default setting set this to the "alarms" node and "totalCount" repectively.

Rest Queries:
===============
The format in which to specify a query is as follows:

<table_name>?query=<the_rest_query>

for example:
    alarms?query=lastEventTime > '2011-08-19 11:11:11.000-07:00'

which means: get the total-count of entries in the alarms table where the
lastEventTime > '2011-08-19 11:11:11.000-07:00'

If, you want to include the "and" operation into your query, you can do so by adding "AND" at the end.

for example:
    alarms?query=lastEventTime > '2011-08-19T11:11:11.000-07:00' AND severity > MAJOR AND alarmAckUser IS NULL

LIMITATION:
    1) OpenNMS allows rest queries to be done on alarms, events, node, ipinterface, or snmpinterface tables. Therefore,
       the extension has been tested with queries run on those tables only.
       http://www.opennms.org/wiki/ReST
    2) If chrome does-not recognize the certificate that is assigned to the OpenNMS machine, it blocks access to that 
       machine until you connect to it and accept it, everytime chrome is loaded. Sine the extension has to run this
       query on the OpenNMS machine, it will not be able to work unless you have connected to it and accepted the exception.
    3) The save button does save the options, however, the options are refreshed only when the time-out for the monitor
       frequency runs out. You can force refresh by disabling and enabling the extension, or reloading it if you are in
       developers mode.
    4) Self-signed certificate - explained in limitation #2, because a self-signed certificate might not be recognised as 
       a valid certificate chrome will not allow the connection to the OpenNMS WebUI. You have to open the opennms web page
       accept the warning message about the self-signed certificate and then reload the extension. This behavior happen everytime
       you close and re-open the browser.
    
IMPORTANT:
    The queries are encoded by the extension in the backend, therefore, there is no need to encode the queries in the 
    options page of the extension.

TODO:
====
    1) Use AngularJS for extension design
    2) Customize for event monitoring
    3) Allow graph monitoring
    
Working On:
===========
    1) Display outages on badge hover



